import logo from "./icon512.png";
import "./App.css";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>You can now start editing your app in you app.js</p>
      </header>
    </div>
  );
}

export default App;
