# Getting Started with Create React App

## Usage

- Clone project
```git clone https://gitlab.com/totol.toolsuite/react-chrome-extension-template```

- Install modules
```yarn install```

- Build chrome-extension directory with the command yarn
```yarn run build-chrome-extension```

- Add it to chrome extension
  - chrome://extensions/
  - Click on "Load unpacked extension"
  - Browse the chrome-extension directory